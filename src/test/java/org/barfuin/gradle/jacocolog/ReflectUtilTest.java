/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of {@link ReflectUtil}.
 */
public class ReflectUtilTest
{
    public static class BadObject
    {
        public void badMethod()
        {
            throw new UnsupportedOperationException("thrown for testing");
        }
    }



    @Test
    public void testCallMethodNull()
    {
        Object result = ReflectUtil.callMethodIfPresent(null, "doesntMatter");
        Assert.assertNull(result);
    }



    @Test
    public void testCallUnknownMethod()
    {
        Object result = ReflectUtil.callMethodIfPresent("some object", "nonexistentMethod");
        Assert.assertNull(result);
    }



    @Test
    public void testCallMethodException()
    {
        Object result = ReflectUtil.callMethodIfPresent(new BadObject(), "badMethod");
        Assert.assertNull(result);
    }
}
