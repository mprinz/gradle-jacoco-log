package org.barfuin.gradle.jacocolog;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Some unit tests of things in {@link LogCoverageTask}.
 */
public class LogCoverageTaskTest
{
    private static LogCoverageTask underTest = null;



    @BeforeClass
    public static void setup()
    {
        Project project = ProjectBuilder.builder().build();
        underTest = project.getTasks().create("underTest", LogCoverageTask.class);
    }



    @Test
    public void testFormatNumber0()
    {
        Assert.assertEquals("42", underTest.formatNumber(0, 42.0123454321789d));
    }



    @Test
    public void testFormatNumber1()
    {
        Assert.assertEquals("42", underTest.formatNumber(1, 42.0123454321789d));
    }



    @Test
    public void testFormatNumber1a()
    {
        Assert.assertEquals("42.1", underTest.formatNumber(1, 42.10123454321789d));
    }



    @Test
    public void testFormatNumber2()
    {
        Assert.assertEquals("42.01", underTest.formatNumber(2, 42.0123454321789d));
    }



    @Test
    public void testFormatNumber10()
    {
        Assert.assertEquals("42.0123454322", underTest.formatNumber(10, 42.0123454321789d));
    }
}
