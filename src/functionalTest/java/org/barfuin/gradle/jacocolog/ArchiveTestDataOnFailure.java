/*
 * Copyright 2019-2023 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.zeroturnaround.zip.ZipUtil;


/**
 * Create a ZIP file with the contents of the test directory in case a functional test fails.
 */
public class ArchiveTestDataOnFailure
    extends TestWatcher
{
    private File testDir = null;

    private File targetZip = null;

    private String outputLog = null;



    public void setTestDir(final File pTestDir)
    {
        testDir = pTestDir;
    }



    public void setTargetZip(final File pTargetZip)
    {
        targetZip = pTargetZip;
    }



    public void setOutputLog(final String pOutputLog)
    {
        outputLog = pOutputLog;
    }



    @Override
    protected void failed(final Throwable pException, final Description pDescription)
    {
        super.failed(pException, pDescription);
        if (outputLog != null) {
            try {
                Files.write(new File(testDir, "build.log").toPath(), outputLog.getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            }
            catch (IOException | RuntimeException e) {
                // Ignore - the build has already failed.
            }
        }
        ZipUtil.pack(testDir, targetZip);
    }
}
